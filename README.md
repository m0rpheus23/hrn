# HRN Tickets

This project requires that you have NodeJs>5, npm>3 and Yarn installed

## How to
1. Clone Repo
2. Run `yarn install` to pull packages
3. Run `npm run serve`
4. Go to `http:\\localhost:3000` to view application
