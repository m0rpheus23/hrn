const express = require('express')
const app = express()

app.use(express.static('dist'))

app.listen(3000, function () {
  console.log('listening on port 3000!. Open your browser and go to http://localhost:3000')
});
