import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'hrn-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  ticketOpen: boolean;

  firstTicket: string;

  constructor() {

  }

  toggleCompare() {
    this.ticketOpen = !this.ticketOpen;
  }

  ngOnInit() {
    this.ticketOpen = false;
    this.firstTicket = `
<li>Cold-pressed poke thundercats brooklyn chillwave chartreuse</li>
<li>Craft beer 3 wolf moon tbh hoodie</li>
<li>YOLO synth hammock</li>
<li>Distillery aesthetic VHS affogato gentrify bespoke </li>
<li>Chia readymade schlitz brunch yuccie echo park</li>
`;
  }


}
