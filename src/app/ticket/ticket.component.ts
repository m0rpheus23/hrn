import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'hrn-ticket',
  templateUrl: './ticket.component.html',
  styleUrls: ['./ticket.component.scss']
})
export class TicketComponent implements OnInit {

  @Input()
  open: boolean;

  @Input()
  isSpecial?: boolean;

  @Input()
  ticketHeader: string;

  @Input()
  ticketAmount: string;

  @Input()
  ticketDate: string;

  @Input()
  ticketMeta: string;

  @Input()
  ticketDetails: string;

  constructor() {
  }

  ngOnInit() {
    this.open = false;
  }

}
